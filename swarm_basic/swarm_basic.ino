#include <NewPing.h>
#include "IRremote.h"
#include "pin_values.h"
// #include <Servo.h>
#include "move_control.h"
// #include "packet.h"

typedef union
{
 long number;
 uint8_t bytes[4];
} FLOATUNION_t;

decode_results ir_cmd;

// Servo myservo;
// int pos = 90;

//Tell the Arduino where the sensor is hooked up
NewPing sonar(SONAR_TRIGGER_PIN, SONAR_ECHO_PIN, MAX_DISTANCE);

// IR receiver pin
IRrecv irrecv(IR_RECV_PIN);
uint32_t ir_value;

// Set motors pins and timeout
MOVE_STATE move_state(pinA1, pinA2, enableA, pinB1, pinB2, enableB, def_timeout);
// long ping_delay = (MAX_DISTANCE/340 * 1000) % 1;
long ping_delay = 15;
// communication package, handle starts from 0.
// PACKET packet(0);

long cm = 0;
unsigned long pingTimer;
boolean newBarrier = true;

// byte header[5];
// byte mv_body[];
// byte sonar_body[];
// byte ir_body[];
// uint8_t move_state_data[2];
// uint8_t move_state_msg[8];

// uint8_t distance_data[6];
// uint8_t distance_msg[12];
// FLOATUNION_t distance_cm;

// uint8_t irr_buffer[5];
// uint8_t irr_msg[10];
// FLOATUNION_t irr_buff_value;


void handle_ir_cmd(int code)
{
  // irr_buff_value.number = results.value
  // irr_buffer = {2, 1};
  // for (byte i=2; i < 6; i += 1) {
  //   irr_buffer[i] = irr_buff_value.bytes[i-2];
  // }
  // packet.pack(irr_buffer, 6, irr_msg);
  // Serial.write(irr_msg, 10);
  if (irrecv.decode(&ir_cmd)) {
    if (ir_cmd.value != 0xFFFFFFFF) {
      ir_value = ir_cmd.value;
    }
    switch (ir_value) {
      case give_sonar:
        Serial.print("ping delay: ");
        Serial.println(ping_delay);
        Serial.print("distance: ");
        Serial.println(cm);
        break;
      case turn_left:
        Serial.println("LEFT");
        move_state.turnLeft();
        break;
      case turn_right:
        Serial.println("RIGHT");
        move_state.turnRight();
        break;
      case speed_up:
        Serial.println("Faster!");
        move_state.faster();
        break;
      case speed_down:
        Serial.println("Slower!");
        move_state.slower();
        break;
      case go_forward:
        Serial.println("GO!");
        move_state.forward();
        break;
      case go_backward:
        Serial.println("BACK");
        move_state.backward();
        break;
      case do_stop:
        Serial.println("STOP");
        move_state.stop();
        break;
      default:
        Serial.println(ir_cmd.value, HEX); 
        break;
    }
    irrecv.resume();
  }
}

// void serialEvent() {
  // if (packet.ready()) {
  //   switch (packet.data[0]) {
  //     case 1:
  //       move_state.change_state(data)
  //     case 2:
  //       switch (data[1]) {
  //         case 1:
  //           if (data[2] == 0) {
  //             distance_cm.number = cm;
  //             distance_data = {2, 1};
  //             for (byte i=2; i < 6; i += 1) {
  //               distance_data[i] = distance_cm.bytes[i-2];
  //             }
  //             packet.pack(distance_data, 6, distance_msg)
  //             Serial.write(move_state_msg, 12)
  //           }
  //         default:
  //           Serial.println(packet.data, HEX)
  //       }
  //     default:
  //       Serial.println(packet.data, HEX)
  //   }
  // }
// }


void echoCheck() { // If ping echo, set distance to array.
  if (sonar.check_timer()) {
    cm = sonar.ping_result/US_ROUNDTRIP_CM;
  }
}


void setup() {
  Serial.begin(115200); // COM spped
  irrecv.enableIRIn(); // Start IR receive
  
  pinMode(enableA, OUTPUT);
  pinMode(pinA1, OUTPUT);
  pinMode(pinA2, OUTPUT);

  pinMode(enableB, OUTPUT);
  pinMode(pinB1, OUTPUT);
  pinMode(pinB2, OUTPUT);

  pingTimer = millis() + ping_delay;
  cm = 0;
  // myservo.attach(SERVO);
}

// bool do_move = true;

void loop() {
  if (irrecv.decode(&ir_cmd)) {
    handle_ir_cmd(ir_cmd.value);
    irrecv.resume(); // Receive the next value
  }

  if (millis() >= pingTimer) {
    sonar.timer_stop();
    sonar.ping_timer(echoCheck);
    pingTimer = millis() + ping_delay;
  }

  if (move_state.on_move && (cm != 0) && (cm < STOP_DIST)) {
    if (newBarrier) {
      Serial.println("Barrier in front of me!");
      newBarrier = false;
    }
    move_state.barrier = (cm != 0) && (cm < STOP_DIST);
  }
  else {
    if (!newBarrier) {
      newBarrier = true;
      Serial.println("Clear!");
    }
    move_state.barrier = (cm == 0) || (cm > STOP_DIST);
  }

  move_state.check_state();
}
