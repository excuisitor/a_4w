const byte enableA = 11;
const byte pinA1 = 8;
const byte pinA2 = 2;

const byte enableB = 7;
const byte pinB1 = 3;
const byte pinB2 = 4;

const unsigned long def_timeout = 100;
const unsigned int STOP_DIST = 35;

const byte IR_RECV_PIN = 6;

// const byte SERVO = 12;

const int MAX_DISTANCE = 250; // Max distance in cm
const byte SONAR_TRIGGER_PIN = 13;
const byte SONAR_ECHO_PIN = 12;

// const uint32_t go_backward = 0x00FF22DD; // |<<
// const uint32_t go_forward = 0x00FF02FD; // >>|
const uint32_t speed_up = 0x00FFA857; // +
const uint32_t speed_down = 0x00FFE01F; // -
const uint32_t do_stop = 0x00FFC23D; // |>||
// const uint32_t turn_left = 0x00FFA25D; // CH-
// const uint32_t turn_right = 0x00FFE21D; // CH+
// const uint32_t go_forward = 0x00FF629D; // CH
const uint32_t give_sonar = 0x00FF906F; // EQ
const uint32_t go_forward = 0x00FF18E7; // 2
const uint32_t turn_left = 0x00FF10EF; // 4
const uint32_t turn_right = 0x00FF5AA5; // 6
const uint32_t go_backward = 0x00FF4AB5; // 8
// 0x00FF38C7 // 5
// 0x00FF30CF // 1
// 0x00FF6897 // 0
// 0x00FF7A85 // 3
// 0x00FF42BD // 7
// 0x00FF52AD // 9
// 0x00FF9867 // +100
// 0x00FFB04F // +200
