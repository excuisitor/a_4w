const word open_seq = 0x7783;


class PACKET {

  private:
    byte position;
    int new_byte;
  
  public:
    byte handle;
    byte length;
    byte data[];

  public:
  PACKET(byte hdl) {
    handle = 0;
    length = 0;
    position = 0;
  }

  public:
  boolean ready() {
    new_byte = Serial.read();

    if (new_byte == -1) {
      return false;
    }

    switch (position) 
    {
      case 0:
        if (new_byte == 0x77) {
          position = 1;
        }
        break;
      case 1:
        if (new_byte == 0x83) {
          position = 2;
        }
        break;
      case 2:
        handle = new_byte;
        position = 3;
        break;
      case 3:
        length = new_byte;
        position = 4;
        break;
      case 4:
        if (handle ^ length == new_byte) {
          position = 5;
          byte *data = new byte[length];
        }
        else {
          // delete [] data;
          position = 0;
        }
        break;
      default:
        if (position - 5 < length) {
          data[position - 5] = new_byte;
          position += 1;
        }
        else {
          position = 0;
          return get_cs(data, length) == new_byte;  
        }
        break;
      }
    return false;
  }

  public:
  void pack(byte *data, byte length, byte *output_buffer) {
    output_buffer[0] = 0x77;
    output_buffer[1] = 0x83;
    handle += 1;
    output_buffer[2] = handle;
    output_buffer[3] = length;
    output_buffer[4] = handle ^ length;

    for (int i = 5; i < length + 5; i += 1) {
      output_buffer[i] = data[i-5];
    }
    output_buffer[length + 5] = get_cs(data, length);
  }

  private:
  byte get_cs(byte check_data[], byte length) {
    byte cs = check_data[0];
    for (int i = 1; i < length; i += 1) {
      cs ^= check_data[i];
    }
    return cs;
  }

};