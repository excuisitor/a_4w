const byte STEP = 15;

// A -- right side
// B -- left side

class MOVE_STATE{

  public:
    boolean on_move = false;
    boolean barrier = false;

  private:
    unsigned long change_start = 0;
    byte APower = 0;
    byte BPower = 0;
    boolean timeLimit = false;

    unsigned long timeout;
    byte pinA1;
    byte pinA2;
    byte pinB1;
    byte pinB2;
    byte enableA;
    byte enableB;

  public:
  MOVE_STATE(
    byte a1,
    byte a2,
    byte enA,
    byte b1,
    byte b2,
    byte enB,
    unsigned long to
  ) {
    pinA1 = a1;
    pinA2 = a2;
    pinB1 = b1;
    pinB2 = b2;
    enableA = enA;
    enableB = enB;
    timeout = to;
  }

  public:
  byte check_state() {    
    if (timeLimit) {
      if (millis() - change_start >= timeout) {
        timeLimit = false;
        stop();
      }
    }

    if (on_move) {
      forward();
    }
  }

  public:
  void forward() {
    if (!barrier) {
      // Serial.println("RUN!");
      byte power = max(APower, BPower);
      setAPower(power);
      setBPower(power);

      motorAForward();
      motorBForward();
    }
    else {
      // Serial.println("Barrier in front of me!");
      brake_();
    }
    on_move = true;
  }

  public:
  void faster() {
    if (APower <= 255 - STEP) {
      APower = (APower + STEP) & 255;
    }
    else {
      APower = 255;
    }

    if (BPower <= 255 - STEP) {
      BPower = (BPower + STEP) & 255;
    }
    else {
      BPower = 255;
    }
    Serial.print("APower: ");
    Serial.println(APower);
    Serial.print("BPower: ");
    Serial.println(BPower);
  }

  public:
  void slower() {
    if (APower >= STEP) {
      APower = (APower - STEP);
    }
    else {
      APower = 0;
    }

    if (BPower >= STEP) {
      BPower = (BPower - STEP);
    }
    else {
      BPower = 0;
    }
    Serial.print("APower: ");
    Serial.println(APower);
    Serial.print("BPower: ");
    Serial.println(BPower);
  }

  public:
  void backward() {
    Serial.println("Back!");
    byte power = max(APower, BPower);
    setAPower(power);
    setBPower(power);
    motorABackward();
    motorBBackward();
    timeLimit = true;
    change_start = millis();
  }

  public:
  void turnRight() {
    if (barrier || (!on_move)) {
      Serial.println("Turn right in place");
      setAPower(255);
      setBPower(255);
      motorABackward();
      motorBForward();
      timeLimit = true;
      change_start = millis();
    }
    else {
      byte power;
      Serial.println("Turn right");
      if (APower >= STEP) {
        power = APower - STEP;
      }
      else {
        power = 0;
      }
      setAPower(power);
    } 
  }

  public:
  void turnLeft() {
    if (barrier || (!on_move)) {
      Serial.println("Turn left in place");
      setAPower(255);
      setBPower(255);
      motorAForward();
      motorBBackward();
      timeLimit = true;
      change_start = millis();
    }
    else {
      Serial.println("Turn left");
      byte power;
      if (BPower >= STEP) {
        power = BPower - STEP;
      }
      else {
        power = 0;
      }
      setBPower(power);
    } 
  }

  public:
  void stop() {
    Serial.println("Stop");
    setAPower(0);
    setBPower(0);
    motorACoast();
    motorBCoast();
    on_move = false;
  }
  
  void brake_() {
    // Serial.println("Brake");
    motorABrake();
    motorBBrake();
  }

    //motor A controls
  void setAPower(byte pow) {
    analogWrite(enableA, pow);
    APower = pow;
  }
  void motorAForward() {
    digitalWrite(pinA1, HIGH);
    digitalWrite(pinA2, LOW);
  }

  void motorABackward() {
    digitalWrite(pinA1, LOW);
    digitalWrite(pinA2, HIGH);
  }

    //motor B controls
  void setBPower(byte pow) {
    analogWrite(enableB, pow);
    BPower = pow;
  }
  void motorBForward() {
    digitalWrite(pinB1, HIGH);
    digitalWrite(pinB2, LOW);
  }

  void motorBBackward() {
    digitalWrite(pinB1, LOW);
    digitalWrite(pinB2, HIGH);
  }

    //coasting and braking
  void motorACoast() {
    digitalWrite(pinA1, LOW);
    digitalWrite(pinA2, LOW);
  }

  void motorABrake() {
    digitalWrite(pinA1, HIGH);
    digitalWrite(pinA2, HIGH);
  }

  void motorBCoast() {
    digitalWrite(pinB1, LOW);
    digitalWrite(pinB2, LOW);
  }

  void motorBBrake() {
    digitalWrite(pinB1, HIGH);
    digitalWrite(pinB2, HIGH);
  }
};